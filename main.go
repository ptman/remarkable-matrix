// Copyright (c) 2022 Paul Tötterman <paul.totterman@iki.fi>

package main

// https://gist.github.com/ptman/e69bd51c0d8be26f635555da33cd18ec
// https://gist.github.com/Utopiah/e2d5c944bbd632e3ae0530e602977f45

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	// nolint:gosec
	"crypto/sha1" 
	"github.com/gofrs/uuid"
	"github.com/kr/pretty"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
)

const (
	cDocumentType = "DocumentType"
	cEPUB         = "epub"
	configKey     = "name.ptman.remarkablebot.config"
	cPDF          = "pdf"
	epubMime      = "application/epub+zip"
	filePerm      = 0644
	pdfMime       = "application/pdf"
	xContent      = ".content"
	xEPUB         = ".epub"
	xMetadata     = ".metadata"
	xPDF          = ".pdf"
	defaultDir    = "/home/root/.local/share/remarkable/xochitl/"
	helpMarkdown  = `
reMarkable matrix bot - https://gitlab.com/ptman/remarkable-matrix

Commands:

- !help - this short help
- !leave - leave this room
- !shutdown - shut down the bot program
- !listfiles - log documents found on remarkable (for dev mostly)
- !dropcache - drop cache of document hashes (for dev mostly)
- !config - manage the config stored in account data
  - dump - show config
  - read - read config from account data
  - save - store config in account data
  - set - set config values

Try attaching .epub or .pdf files.
`
)

type config struct {
	AccessToken string    `yaml:"accessToken"`
	Admin       id.UserID `yaml:"admin"`
	Dir         string    `yaml:"dir"`
	hsURI       string
	Password    string `yaml:"password"`
	userID      id.UserID
	UserID      string `yaml:"userID"` //nolint:tagliatelle
	username    string
}

func (c *config) parse() error {
	c.userID = id.UserID(c.UserID)

	local, hs, err := c.userID.ParseAndValidate()
	if err != nil {
		return errors.WithStack(err)
	}

	c.username = local

	wellknown, err := mautrix.DiscoverClientAPI(hs)
	if err != nil {
		return errors.WithStack(err)
	}

	c.hsURI = wellknown.Homeserver.BaseURL

	return nil
}

func (c *config) defaults() {
	if c.Dir == "" {
		c.Dir = defaultDir
	}
}

type accountConfig struct {
	LogLevel int // grep for LogLevel to see what the levels are
	Dir      string
	Admin    id.UserID
}

func (c *accountConfig) override(conf *config) {
	if conf.Dir != "" {
		c.Dir = conf.Dir
	}

	if conf.Admin != "" {
		c.Admin = conf.Admin
	}
}

type documentMetadata struct {
	Deleted          bool   `json:"deleted,omitempty"`
	LastModifiedUnix int64  `json:"lastModified,string,omitempty"`
	LastOpenedUnix   int64  `json:"lastOpened,string,omitempty"`
	LastOpenedPage   int    `json:"lastOpenedPage,omitempty"`
	MetadataModified bool   `json:"metadatamodified,omitempty"`
	Parent           string `json:"parent"`
	Pinned           bool   `json:"pinned,omitempty"`
	Synced           bool   `json:"synced,omitempty"`
	Type             string `json:"type,omitempty"`
	Version          int    `json:"version,omitempty"`
	VisibleName      string `json:"visibleName,omitempty"`
}

type documentContent struct {
	CoverPageNumber  int            `json:"coverPageNumber,omitempty"`
	DocumentMetadata struct{}       `json:"documentMetadata,omitempty"`
	DummyContent     bool           `json:"dummyContent,omitempty"`
	ExtraMetadata    struct{}       `json:"extraMetadata,omitempty"`
	FileType         string         `json:"fileType,omitempty"`
	FontName         string         `json:"fontName,omitempty"`
	LineHeight       int            `json:"lineHeight,omitempty"`
	Margins          int            `json:"margins,omitempty"`
	Orientation      string         `json:"orientation,omitempty"`
	PageCount        int            `json:"pageCount,omitempty"`
	Pages            []string       `json:"pages,omitempty"`
	TextAlignment    string         `json:"textAlignment,omitempty"`
	TextScale        int            `json:"textScale,omitempty"`
	Transform        map[string]int `json:"transform,omitempty"`
}

func (m *documentMetadata) LastModified() time.Time {
	//nolint:gomnd
	return time.Unix(m.LastModifiedUnix/1000, m.LastModifiedUnix%1000)
}

func (m *documentMetadata) LastOpened() time.Time {
	//nolint:gomnd
	return time.Unix(m.LastOpenedUnix/1000, m.LastOpenedUnix%1000)
}

//nolint:gochecknoglobals
var (
	store  *mautrix.InMemoryStore
	client *mautrix.Client

	aconf accountConfig
	conf  *config

	hashcache map[string]string
)

func init() { //nolint:gochecknoinits
	hashcache = make(map[string]string)
}

func restartXochitl() {
	cmd := exec.Command("systemctl", "restart", "xochitl")
	if err := cmd.Run(); err != nil {
		log.Printf("%+v", err)
	}
}

func processCommand(e *event.Event, fields []string, body string) {
	switch strings.ToLower(fields[0][1:]) {
	case "config":
		processConfig(e, fields[1:])
	case "help":
		content := format.RenderMarkdown(helpMarkdown, true, true)
		content.MsgType = event.MsgNotice

		_, err := client.SendMessageEvent(e.RoomID, event.EventMessage,
			content)
		errLog(err)
	case "dropcache":
		if e.Sender != aconf.Admin {
			return
		}

		for x := range hashcache {
			delete(hashcache, x)
		}
	case "shutdown":
		if e.Sender != aconf.Admin {
			return
		}

		os.Exit(0)
	case "listfiles":
		processListfiles(e, fields[1:])
	case "leave":
		processLeave(e, fields[1:])
	default:
		_, err := client.SendNotice(e.RoomID, fmt.Sprintf(
			"unknown command: %s", fields[0]))
		errLog(err)
	}
}

func hashFile(key, path string) string {
	if h, ok := hashcache[key]; ok {
		return h
	}

	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	h := sha1.New() //nolint:gosec
	if _, err := io.Copy(h, f); err != nil {
		panic(err)
	}

	hashcache[key] = fmt.Sprintf("%x", h.Sum(nil))

	return hashcache[key]
}

type rmfile struct {
	UUID uuid.UUID
	Path string
	Name string
	Hash string
}

func getFileList() (list []rmfile) {
	start := time.Now()

	files, err := os.ReadDir(aconf.Dir)
	if err != nil {
		errLog(err)

		return list
	}

	list = make([]rmfile, 0, len(files))

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		name := file.Name()

		if !(strings.HasSuffix(name, xPDF) ||
			strings.HasSuffix(name, xEPUB)) {
			continue
		}

		base := strings.TrimSuffix(name, xPDF)
		base = strings.TrimSuffix(base, xEPUB)

		uid, err := uuid.FromString(base)
		if err != nil {
			errLog(err)

			continue
		}

		basepath := filepath.Join(aconf.Dir, base)

		metadatapath := basepath + xMetadata
		fullpath := filepath.Join(aconf.Dir, name)

		b, err := os.ReadFile(metadatapath)
		if err != nil {
			errLog(err)

			continue
		}

		metadata := &documentMetadata{} //nolint:exhaustivestruct
		if err := json.NewDecoder(bytes.NewReader(b)).Decode(metadata); err != nil {
			errLog(err)

			continue
		}

		h := hashFile(base, fullpath)

		list = append(list, rmfile{
			UUID: uid,
			Hash: h,
			Path: fullpath,
			Name: metadata.VisibleName,
		})
	}

	log.Printf("%s", time.Since(start))

	return list
}

func processListfiles(e *event.Event, fields []string) {
	if e.Sender != aconf.Admin {
		return
	}

	list := getFileList()

	for _, file := range list {
		log.Printf("%s %s %s", file.UUID.String(), file.Hash, file.Name)
	}
}

func processConfig(e *event.Event, fields []string) {
	if e.Sender != aconf.Admin {
		return
	}

	switch strings.ToLower(fields[0]) {
	case "read":
		errLog(client.GetAccountData(configKey, &aconf))
	case "save":
		errLog(client.SetAccountData(configKey, aconf))
	case "dump":
		content := format.RenderMarkdown(fmt.Sprintf("```\n%# v\n```",
			pretty.Formatter(aconf)), true, true)

		_, err := client.SendMessageEvent(e.RoomID, event.EventMessage,
			content)
		errLog(err)
	case "set":
		processSet(e, fields[1:])
	default:
		_, err := client.SendNotice(e.RoomID, fmt.Sprintf(
			"unknown command: %s", fields[0]))
		errLog(err)
	}
}

func processLeave(e *event.Event, fields []string) {
	_, err := client.LeaveRoom(e.RoomID,
		&mautrix.ReqLeave{Reason: "asked to"})
	errLog(err)

	_, err = client.ForgetRoom(e.RoomID)
	errLog(err)
}

func processSet(e *event.Event, fields []string) {
	if e.Sender != aconf.Admin {
		return
	}

	switch fields[0] {
	case "loglevel":
		i, err := strconv.Atoi(fields[1])
		errLog(err)

		aconf.LogLevel = i
	case "dir":
		aconf.Dir = fields[1]
	case "admin":
		admin := id.UserID(fields[1])

		_, _, err := admin.ParseAndValidate()
		if err != nil {
			errLog(err)

			return
		}

		aconf.Admin = admin
	default:
		_, err := client.SendNotice(e.RoomID,
			fmt.Sprintf("unknown setting: %s", fields[0]))
		errLog(err)
	}
}

func processFile(e *event.Event) {
	if e.Sender != aconf.Admin {
		return
	}

	message := e.Content.AsMessage()
	name := strings.TrimSpace(message.Body)

	var fileType string

	switch message.Info.MimeType {
	case pdfMime:
		fileType = cPDF
	case epubMime:
		fileType = cEPUB
	default:
		log.Printf("%s %s", name, message.Info.MimeType)

		return
	}

	f, err := os.CreateTemp("", "mx")
	if err != nil {
		errLog(err)

		return
	}

	defer os.Remove(f.Name())
	defer f.Close()

	uri, err := message.URL.Parse()
	if err != nil {
		errLog(err)

		return
	}

	r, err := client.Download(uri)
	if err != nil {
		errLog(err)

		return
	}

	if _, err := io.Copy(f, r); err != nil {
		errLog(err)

		return
	}

	if err := r.Close(); err != nil {
		errLog(err)

		return
	}

	_, err = f.Seek(0, 0)
	errLog(err)

	uid, err := uuid.NewV4()
	if err != nil {
		errLog(err)

		return
	}

	list := getFileList()
	h := hashFile(uid.String(), f.Name())

	for _, file := range list {
		if h == file.Hash {
			log.Printf("%s duplicate of %s %s", h,
				file.UUID.String(), file.Name)

			return
		}
	}

	basepath := filepath.Join(aconf.Dir, uid.String())

	stat, err := os.Stat(fmt.Sprintf("%s%s", basepath, xMetadata))
	if err == nil {
		log.Printf("file exists %+v", stat)

		return
	} else if os.IsNotExist(err) {
		// ok
	} else if err != nil {
		errLog(err)

		return
	}

	fn := fmt.Sprintf("%s.%s", basepath, fileType)
	stat, err = os.Stat(fn)
	if err == nil {
		log.Printf("file exists %+v", stat)

		return
	} else if os.IsNotExist(err) {
		// ok
	} else if err != nil {
		errLog(err)

		return
	}

	n, err := os.Create(fn)
	if err != nil {
		errLog(err)

		return
	}

	if _, err := io.Copy(n, f); err != nil {
		errLog(err)

		return
	}

	md := documentMetadata{ //nolint:exhaustivestruct
		VisibleName: name,
		Type:        cDocumentType,
		Parent:      "",
	}

	b, err := json.Marshal(md)
	if err != nil {
		errLog(err)

		return
	}

	if err := os.WriteFile(fmt.Sprintf("%s%s", basepath, xMetadata), b,
		filePerm); err != nil {
		errLog(err)

		return
	}

	dc := documentContent{ //nolint:exhaustivestruct
		FileType: fileType,
	}

	b, err = json.Marshal(dc)
	if err != nil {
		errLog(err)

		return
	}

	if err := os.WriteFile(fmt.Sprintf("%s%s", basepath, xContent), b,
		filePerm); err != nil {
		errLog(err)

		return
	}

	log.Printf("%s %s %d %s", uid.String(), fileType, message.Info.Size,
		name)

	_ = getFileList() // update

	restartXochitl()
}

func readConfig() error {
	fn := "config.yaml"
	if len(os.Args) > 1 {
		fn = os.Args[1]
	}

	f, err := os.Open(fn)
	if err != nil {
		return errors.WithStack(err)
	}

	conf = &config{} //nolint:exhaustivestruct

	if err := yaml.NewDecoder(f).Decode(conf); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func errPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func errLog(err error) {
	if err != nil {
		log.Printf("%+v", err)
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	errPanic(readConfig())

	errPanic(conf.parse())

	conf.defaults()

	client, err := mautrix.NewClient(conf.hsURI, conf.userID,
		conf.AccessToken)
	errPanic(err)

	if client.AccessToken == "" {
		//nolint:exhaustivestruct
		_, err = client.Login(&mautrix.ReqLogin{
			Type: mautrix.AuthTypePassword,
			//nolint:exhaustivestruct
			Identifier: mautrix.UserIdentifier{
				Type: mautrix.IdentifierTypeUser,
				User: conf.username,
			},
			Password:         conf.Password,
			StoreCredentials: true,
		})
		if err != nil {
			panic(err)
		}

		log.Printf("accessToken: %s", client.AccessToken)
	}

	errLog(client.GetAccountData(configKey, &aconf))

	aconf.override(conf)

	store = mautrix.NewInMemoryStore()

	syncer, ok := client.Syncer.(*mautrix.DefaultSyncer)
	if !ok {
		log.Printf("not DefaultSyncer")
	}

	syncer.OnSync(func(resp *mautrix.RespSync, since string) bool {
		for roomID, evts := range resp.Rooms.Join {
			room := store.LoadRoom(roomID)
			if room == nil {
				room = mautrix.NewRoom(roomID)
				store.SaveRoom(room)
			}
			for _, i := range evts.State.Events {
				room.UpdateState(i)
			}
			for _, i := range evts.Timeline.Events {
				if i.Type.IsState() {
					room.UpdateState(i)
				}
			}
		}

		return true
	})
	syncer.OnEvent(func(source mautrix.EventSource, e *event.Event) {
		switch {
		case aconf.LogLevel > 0: // don't log own messages
			if e.Sender == conf.userID {
				break
			}
		case aconf.LogLevel > 1: // don't log ephemeral messages
			if e.Type.IsEphemeral() {
				break
			}
		default:
			log.Printf("(%s) %# v", source, pretty.Formatter(e))
		}

		if source&mautrix.EventSourceJoin != 0 && source&mautrix.EventSourceTimeline != 0 {
			errPanic(client.MarkRead(e.RoomID, e.ID))
		}
	})
	syncer.OnEventType(event.StateMember, func(source mautrix.EventSource, e *event.Event) {
		if e.Content.AsMember().Membership != event.MembershipInvite {
			return
		}

		if e.GetStateKey() != conf.UserID {
			return
		}

		if e.Sender != aconf.Admin {
			return
		}

		_, err := client.JoinRoomByID(e.RoomID)
		errLog(err)
	})
	syncer.OnEventType(event.EventMessage, func(source mautrix.EventSource, e *event.Event) {
		if e.Sender == conf.userID {
			return
		}

		message := e.Content.AsMessage()
		if message.MsgType == event.MsgNotice {
			return
		}

		if message.MsgType == event.MsgFile {
			processFile(e)

			return
		}

		body := strings.TrimSpace(message.Body)
		log.Printf("%s: %s", e.Sender, body)

		fields := strings.Fields(body)
		if strings.HasPrefix(fields[0], "!") {
			processCommand(e, fields, body)
		}
	})

	eventIgnorer := mautrix.OldEventIgnorer{UserID: conf.userID}
	eventIgnorer.Register(syncer)

	errPanic(client.Sync())
}
