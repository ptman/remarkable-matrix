# reMarkable matrix bot

For the longest time I wanted to be able to email documents to my [reMarkable
(referral link)](https://remarkable.com/referral/B3NW-4LBN) like I can to my
kindle. I even hacked it together with rmapi at some point. But it was never
perfect.

Recently reMarkable released the Google Drive integration. That works.

Then someone made a post about a telegram bot for their reMarkable. But I prefer
[matrix](https://matrix.org) to telegram. And the bot was written in javascript,
so deployment was a pain. Go can easily cross-build statically linked binaries.
Deployment is binary + config file.

## Usage

1. Build binary (`make build-rm`)
2. Edit config file.
3. Copy config + binary to reMarkable (using USB networking and SSH)
4. Start bot
5. Open chat with bot
6. Attach PDF or EPUB file
7. You can try asking the bot for `!help`

## TODO

- e2ee
- better feedback
- more useful features
- tests
- ...

## Contact

I'm [@ptman:ptman.name](https://matrix.to/#/@ptman:ptman.name)

## License

ISC
