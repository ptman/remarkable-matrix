help:
	@cat Makefile
run:
	CGO_ENABLED=0 go run main.go

build-rm:
	GOOS=linux GOARCH=arm GOARM=7 CGO_ENABLED=0 go build -tags timetzdata -trimpath -ldflags "-s -w"

lint:
	CGO_ENABLED=0 golangci-lint run --enable-all .

clean:
	rm -f remarkablebot
