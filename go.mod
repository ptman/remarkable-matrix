module remarkablebot

go 1.17

require (
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/kr/pretty v0.3.0
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	maunium.net/go/mautrix v0.10.11
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
)
